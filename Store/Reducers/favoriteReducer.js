
const initialState = { favoritesFilm: [] }

function toggleFavorite(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'TOGGLE_FAVORITE':
            //La constante favoritesFilmIndex va récupérer les données du tableau favoriteFilm 
            const favoritesFilmIndex = state.favoritesFilm.findIndex(item => item.id === action.value.id)
            if (favoritesFilmIndex !== -1){
                //Le film est déjà dans les favoris, on le suprime de la liste
                nextState = {
                    ...state,
                    favoritesFilm: state.favoritesFilm.filter((item, index) => index !== favoritesFilmIndex )
                }
            }
            else {
                //Le film n'est pas dans la liste des favoris, on l'ajoute à la liste
                nextState = {
                    ...state,
                    favoritesFilm: [...state.favoritesFilm, action.value]
                }
            }
            //On renvoie l'objet nextState si celui-ci n'est pas undefined, sinon on renvoie l'objet state
            return nextState || state
        default:
            return state
    }
}

export default toggleFavorite
